﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Core.CoreServices.Categories;
using UTB.EShop.Core.CoreServices.Files;
using UTB.EShop.Core.CoreServices.Products;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Core.Models.Products;
using UTB.EShop.Web.Areas.Admin.AppServices.Products;

namespace UTB.EShop.Web.Tests.AppServices.Products
{
	[TestClass]
	public class ProductAppServiceUnitTests
	{
		private static IProductAppService _productAppService;
		private const int _productID = 1;

		[ClassInitialize]
		public static void ClassInitialize(TestContext context)
		{
			var mockProductCoreService = new Mock<IProductCoreService>();
			mockProductCoreService.Setup(s => s.GetAll())
				.Returns(new List<Product>
				{
					new Product
					{
						ProductID = 1,
						DateCreated = DateTime.Now,
						CreatedBy = 0,
						DateUpdated = DateTime.Now,
						UpdatedBy = 0,
						Name = "Párek",
						Price = 15.6
					},
					new Product
					{
						ProductID = 2,
						DateCreated = DateTime.Now,
						CreatedBy = 0,
						DateUpdated = DateTime.Now,
						UpdatedBy = 0,
						Name = "Rohlík",
						Price = 2.5
					},
					new Product
					{
						ProductID = 3,
						DateCreated = DateTime.Now,
						CreatedBy = 0,
						DateUpdated = DateTime.Now,
						UpdatedBy = 0,
						Name = "Kečup",
						Price = 31.29
					}
				});

			var mockCategoryCoreService = new Mock<ICategoryCoreService>();
			mockCategoryCoreService.Setup(s => s.GetAll())
				.Returns(new List<Category>
				{
					new Category
					{
						CategoryID =  1,
						Name = "Oblečení"
					},
					new Category
					{
						CategoryID =  2,
						Name = "Elektronika"
					}
				});

			_productAppService = new ProductAppService(
									mockCategoryCoreService.Object,
									mockProductCoreService.Object,
									new FileResolver(null));
		}

		[TestMethod]
		public void GetAllViewModel_MockProductService()
		{
			// Arrange


			// Act
			var productsViewModel = _productAppService.GetAllViewModel();

			// Assert
			Assert.IsNotNull(productsViewModel);
			Assert.IsNotNull(productsViewModel.Products);
			Assert.AreEqual(3, productsViewModel.Products.Count);

		}

		[TestMethod]
		public void GetAllViewModel_WithProductService()
		{
			// Arrange
			var productAppService = new ProductAppService(
										new CategoryCoreService(new DataContext()), 
										new ProductCoreService(new DataContext(), new SessionStateHandler()),
										new FileResolver(null));

			// Act
			var productsViewModel = productAppService.GetAllViewModel();

			// Assert
			Assert.IsNotNull(productsViewModel);
			Assert.IsNotNull(productsViewModel.Products);
			Assert.AreEqual(5, productsViewModel.Products.Count);

		}

		[TestMethod]
		public void GetViewModel_MockProductService()
		{
			// Arrange

			// Act
			var productViewModel = _productAppService.GetViewModel(_productID);

			// Assert
			Assert.IsNotNull(productViewModel);
			Assert.IsNotNull(productViewModel.Product);
			Assert.AreEqual(_productID, productViewModel.Product.ProductID);
			Assert.IsNull(productViewModel.Product.Categories);
		}

		[TestMethod]
		public void GetViewModel_WithProductService()
		{
			// Arrange
			var productAppService = new ProductAppService(
										new CategoryCoreService(new DataContext()),
										new ProductCoreService(new DataContext(), new SessionStateHandler()),
										new FileResolver(null));

			// Act
			var productViewModel = productAppService.GetViewModel(_productID);

			// Assert
			Assert.IsNotNull(productViewModel);
			Assert.IsNotNull(productViewModel.Product);
			Assert.AreEqual(_productID, productViewModel.Product.ProductID);
			Assert.IsNotNull(productViewModel.Product.Categories);
		}
	}
}