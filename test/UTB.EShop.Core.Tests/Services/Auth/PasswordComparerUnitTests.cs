﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UTB.EShop.Core.CoreServices.Auth;

namespace UTB.EShop.Core.Tests.Services.Auth
{
	[TestClass]
	public class PasswordComparerUnitTests
	{
		[TestMethod]
		public void Matches_ValidTest()
		{
			//Arrange
			var testedPassword = "MojeHeslo";
			var password = "MojeHesloSalt";
			var passwordSalt = "Salt";
			var comparer = new PasswordComparer();

			//Act
			var result = comparer.Matches(testedPassword, password, passwordSalt);

			//Assert
			Assert.AreEqual(true, result);
		}

		[TestMethod]
		public void Matches_InvalidTest()
		{
			//Arrange
			var testedPassword = "MojeHeslicko";
			var password = "MojeHesloSalt";
			var passwordSalt = "Salt";
			var comparer = new PasswordComparer();

			//Act
			var result = comparer.Matches(testedPassword, password, passwordSalt);

			//Assert
			Assert.AreEqual(false, result);
		}
	}
}