﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Core.CoreServices.Products;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.Tests.Services.Products
{
	[TestClass]
	public class ProductCoreServiceUnitTests
	{
		[TestMethod]
		public void GetProducts_WithDataContext()
		{
			// Arrange
			var productCoreService = new ProductCoreService(new DataContext(), new SessionStateHandler());

			// Act
			var products = productCoreService.GetAll();

			// Assert
			Assert.AreEqual(5, products.Count);	// can fails, as this test goes against live data
		}

		[TestMethod]
		public void GetProducts_MockDataContext()
		{
			// Arrange
			var data = new List<Product>
			{
				new Product
				{
					ProductID = 1,
					DateCreated = DateTime.Now,
					CreatedBy = 0,
					DateUpdated = DateTime.Now,
					UpdatedBy = 0,
					Name = "Párek",
					Price = 15.6
				},
				new Product
				{
					ProductID = 2,
					DateCreated = DateTime.Now,
					CreatedBy = 0,
					DateUpdated = DateTime.Now,
					UpdatedBy = 0,
					Name = "Rohlík",
					Price = 2.5
				},
				new Product
				{
					ProductID = 3,
					DateCreated = DateTime.Now,
					CreatedBy = 0,
					DateUpdated = DateTime.Now,
					UpdatedBy = 0,
					Name = "Kečup",
					Price = 31.29
				}
			}.AsQueryable();

			var moqProducts = new Mock<DbSet<Product>>();
			moqProducts.As<IQueryable<Product>>()
				.Setup(m => m.Provider)
				.Returns(data.Provider);
			moqProducts.As<IQueryable<Product>>()
				.Setup(m => m.Expression)
				.Returns(data.Expression);
			moqProducts.As<IQueryable<Product>>()
				.Setup(m => m.ElementType)
				.Returns(data.ElementType);
			moqProducts.As<IQueryable<Product>>()
				.Setup(m => m.GetEnumerator())
				.Returns(data.GetEnumerator());
			moqProducts.Setup(m => m.AsNoTracking())
				.Returns(moqProducts.Object);

			var mockContext = new Mock<DataContext>();
			mockContext.Setup(c => c.Products)
				.Returns(moqProducts.Object);
			var productAppService = new ProductCoreService(mockContext.Object, new SessionStateHandler());

			// Act
			var products = productAppService.GetAll();

			// Assert
			Assert.IsNotNull(products);
			Assert.AreEqual(3, products.Count);
			Assert.AreEqual(1, products[0].ProductID);
			Assert.AreEqual("Rohlík", products[1].Name);
			Assert.AreEqual(351.00, products[0].Price);		// will fail
		}
	}
}