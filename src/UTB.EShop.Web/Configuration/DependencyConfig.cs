﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;

namespace UTB.EShop.Web
{
	public class DependencyConfig
	{
		private static ContainerBuilder _builder;

		public DependencyConfig()
		{
			_builder = new Autofac.ContainerBuilder();

			Configure();
		}

		public void Configure()
		{
			// System.Diagnostics.Debugger.Break();

			// ######################################## BUILDER ########################################
			Assembly assembly;

			_builder
				.RegisterSource(new Autofac.Integration.Mvc.ViewRegistrationSource());

			_builder
				.RegisterModule(new Autofac.Integration.Mvc.AutofacWebTypesModule());

			// DATACONTEXT
			_builder
				.RegisterType<Core.EntityFramework.DataContext>()
				.AsSelf()
				.InstancePerLifetimeScope();

			// UTB.ESHOP.CORE
			assembly = Assembly.GetAssembly(typeof(UTB.EShop.Core.Models.Products.Product));
			_builder
				.RegisterAssemblyTypes(assembly)
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();

			_builder
				.RegisterType<UTB.EShop.Core.CoreServices.Orders.OrderedOrderNumberGenerator>()
				.As<UTB.EShop.Core.CoreServices.Orders.IOrderNumberGenerator>()
				.InstancePerLifetimeScope();

			// UTB.ESHOP.WEB (ADMIN & ESHOP & .Web/root)
			assembly = Assembly.GetAssembly(typeof(UTB.EShop.Web.Areas.Admin.Models.ProductViewModel));
			_builder
				.RegisterAssemblyTypes(assembly)
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();
			_builder
				.RegisterControllers(assembly);

			// FluentValidation -- registruje jednotlive ~ViewModelValidator-y do builderu
			var serviceKey = typeof(FluentValidation.IValidator<UTB.EShop.Web.Areas.Admin.Models.ProductViewModel>);
			_builder
				.RegisterType<UTB.EShop.Web.Areas.Admin.Validators.Products.ProductViewModelValidator>()
				.Keyed<FluentValidation.IValidator>(serviceKey)
				.As<FluentValidation.IValidator>();

			serviceKey = typeof(FluentValidation.IValidator<UTB.EShop.Web.Areas.Eshop.Models.CartViewModel>);
			_builder
				.RegisterType<UTB.EShop.Web.Areas.Eshop.Validators.Carts.CartViewModelValidator>()
				.Keyed<FluentValidation.IValidator>(serviceKey)
				.As<FluentValidation.IValidator>();
		}

		public void Build()
		{
			// ######################################## CONTAINER ########################################
			IContainer container = _builder.Build();

			System.Web.Mvc.DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

			// FluentValidation - registruje se v ramci containeru
			System.Web.Mvc.ModelValidatorProviders.Providers.Clear();
			System.Web.Mvc.DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

			var validatorProvider = new FluentValidation.Mvc.FluentValidationModelValidatorProvider(new UTB.EShop.Core.Validators.ValidatorFactory(container))
			{
				AddImplicitRequiredValidator = false
			};

			System.Web.Mvc.ModelValidatorProviders.Providers.Add(validatorProvider);
		}
	}
}