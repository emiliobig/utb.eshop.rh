﻿using System.Web.Mvc;
using System.Web.Routing;

namespace UTB.EShop.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
				namespaces: new[] { "UTB.EShop.Web.Areas.Eshop.Controllers" }
			).DataTokens.Add("area", "Eshop");
		}
	}
}
