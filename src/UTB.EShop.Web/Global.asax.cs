﻿using System;
using System.Globalization;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace UTB.EShop.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			new DependencyConfig().Build();
		}

		// CultureInfo
		protected void Application_BeginRequest()
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("cs-CZ");
		}

		// Error handling - Main
		protected void Application_Error()
		{
			var exception = Server.GetLastError();
			Response.Clear();

			var httpCode = GetExceptionStatusCode(exception);
			switch (httpCode)
			{
				case HttpStatusCode.NotFound:
				case HttpStatusCode.BadRequest:
					// TODO: log error
					ReturnToError(HttpStatusCode.NotFound);
					return;
				default:
					// TODO: log error
					ReturnToError(HttpStatusCode.InternalServerError);
					return;
			}
		}

		// Error handling - from HttpException returns HttpStatusCode
		private HttpStatusCode GetExceptionStatusCode(Exception exception)
		{
			var httpException = exception as HttpException;
			if (httpException == null)
			{
				return HttpStatusCode.InternalServerError;
			}

			switch (httpException.GetHttpCode())
			{
				case (int)HttpStatusCode.BadRequest:
					return HttpStatusCode.BadRequest;
				case (int)HttpStatusCode.NotFound:
					return HttpStatusCode.NotFound;
				default:
					return HttpStatusCode.InternalServerError;
			}
		}

		// Error handling - in RELEASE mode returns custom ErrorXXX, in DEBUG mode returns ISS error page
		private void ReturnToError(HttpStatusCode statusCode)
		{
#if !DEBUG
			switch (statusCode)
			{
				case HttpStatusCode.NotFound:
					Response.StatusCode = (int) HttpStatusCode.NotFound;
					break;
				default:
					Response.StatusCode = (int) HttpStatusCode.InternalServerError;
					break;
			}

			Server.ClearError();
			Response.Redirect($"~/Error/Error{statusCode:d}");
#endif
		}
	}
}