﻿using System.Web.Mvc;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Web.App.UI
{
	// https://haacked.com/archive/2011/02/21/changing-base-type-of-a-razor-view.aspx/
	public class WebViewPageExt<TModel> : WebViewPage<TModel>
	{
		// Currently logged user. Null if no user is logged
		public User CurrentUser => DependencyResolver.Current.GetService<ISessionStateHandler>().GetCurrentUser();

		// Is current user authenticated?
		public bool IsAuthenticated => DependencyResolver.Current.GetService<IFormAuthentication>().IsAuthenticated();

		public override void Execute()
		{
		}
	}
}