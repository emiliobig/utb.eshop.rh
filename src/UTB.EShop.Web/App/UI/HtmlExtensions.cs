﻿using System.Web;
using System.Web.Mvc;

namespace UTB.EShop.Web.App.UI
{
	public static class HtmlExtensions
	{
		public static MvcHtmlString MenuItem(this HtmlHelper<dynamic> helper, string displayText, string actionName, string controllerName, object parameters = null)
		{
			var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
			var url = urlHelper.Action(actionName, controllerName, parameters);

			var link = new TagBuilder("a");
			link.MergeAttribute("href", url);
			link.SetInnerText(displayText);

			link.AddCssClass("menu-item");

			if (IsMenuItemActive(actionName, controllerName, parameters))
				link.AddCssClass("active");

			return new MvcHtmlString(link.ToString());
		}

		private static bool IsMenuItemActive(string actionName, string controllerName, object parameters)
		{
			var action = (string)HttpContext.Current.Request.RequestContext.RouteData.Values["action"];
			var controller = (string)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"];

			object area;

			HtmlHelper.ObjectToDictionary(parameters).TryGetValue("area", out area);

			return action == actionName
				   && controller == controllerName
				   && area == null;
		}
	}
}