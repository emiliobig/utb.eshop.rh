﻿using System.Web.Mvc;

namespace UTB.EShop.Web.Areas.Eshop.Controllers
{
	public class HomeController : Controller
	{
		// GET: Eshop/Home
		public ActionResult Index()
		{
			return View();
		}
	}
}