﻿using System.Web.Mvc;
using UTB.EShop.Web.Areas.Eshop.AppServices.Products;

namespace UTB.EShop.Web.Areas.Eshop.Controllers
{
	public class ProductsController : Controller
	{
		private readonly IProductAppService _productAppService;

		// COTR
		public ProductsController(IProductAppService productAppService)
		{
			_productAppService = productAppService;
		}

		// GET: Eshop/Products
		public ActionResult Index()
		{
			var vm = _productAppService.GetAllViewModel();

			return View(vm);
		}

		[HttpGet]
		public ActionResult Detail(int productID)
		{
			var vm = _productAppService.GetViewModel(productID);

			if (vm.Product == null)
			{
				return RedirectToAction("Error404", "Error");
			}

			return View(vm);
		}
	}
}