﻿using System.Web.Mvc;
using UTB.EShop.Web.Areas.Eshop.AppServices.Carts;
using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.Controllers
{
	public class CartController : Controller
	{
		private readonly ICartAppService _cartAppService;

		public CartController(ICartAppService cartAppService)
		{
			_cartAppService = cartAppService;
		}

		// GET: Eshop/Cart
		public ActionResult Index()
		{
			var vm = _cartAppService.GetViewModel();

			return View(vm);
		}

		[HttpPost]
		public JsonResult AddToCart(int productID, int amount)
		{
			var cartItem = _cartAppService.AddToCart(productID, amount);

			return Json(new
			{
				cartItem.CartID,
				cartItem.CartItemID,
				cartItem.ProductID,
				cartItem.Amount
			});
		}

		[HttpPost]
		public JsonResult RemoveFromCart(int productID)
		{
			_cartAppService.RemoveFromCart(productID);

			return Json(string.Empty);
		}

		// STEPs
		[HttpGet]
		public ActionResult Step1()
		{
			var vm = _cartAppService.GetViewModel(1);

			return View(vm);
		}

		[HttpPost]
		public ActionResult Step1(CartViewModel vm)
		{
			if (!ModelState.IsValid)
			{
				return View(vm);
			}

			_cartAppService.InsertAddresses(vm);

			return RedirectToAction("Step2");
		}

		[HttpGet]
		public ActionResult Step2()
		{
			var vm = _cartAppService.GetViewModel(2);

			return View(vm);
		}

		[HttpPost]
		public ActionResult Step2(CartViewModel vm)
		{
			if (!ModelState.IsValid)
			{
				return View(vm);
			}

			_cartAppService.InsertPaymentAndDelivery(vm);

			return RedirectToAction("Step3");
		}

		[HttpGet]
		public ActionResult Step3()
		{
			var vm = _cartAppService.GetViewModel(3);

			return View(vm);
		}
	}
}