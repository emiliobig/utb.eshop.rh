﻿using System.Web.Mvc;

namespace UTB.EShop.Web.Areas.Eshop.Controllers
{
	public class ErrorController : Controller
	{
		// GET: Eshop/Error/Error404
		public ViewResult Error404()
		{
			return View();
		}

		// GET: Eshop/Error/Error500
		public ViewResult Error500()
		{
			return View();
		}
	}
}