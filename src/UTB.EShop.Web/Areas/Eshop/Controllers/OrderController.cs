﻿using System.Web.Mvc;
using UTB.EShop.Web.Areas.Eshop.AppServices.Orders;

namespace UTB.EShop.Web.Areas.Eshop.Controllers
{
    public class OrderController : Controller
    {
	    private readonly IOrderAppService _orderAppService;

	    public OrderController(IOrderAppService orderAppService)
	    {
		    _orderAppService = orderAppService;
	    }

	    public RedirectToRouteResult Create()
	    {
		    _orderAppService.CreateOrderFromCart();

		    return RedirectToAction("Success");
	    }

	    public ViewResult Success()
	    {
		    return View();
	    }
	}
}