﻿namespace UTB.EShop.Web.Areas.Eshop.AppServices.Orders
{
	public interface IOrderAppService
	{
		void CreateOrderFromCart();
	}
}