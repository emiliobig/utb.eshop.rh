﻿using UTB.EShop.Core.CoreServices.Carts;
using UTB.EShop.Core.CoreServices.Orders;

namespace UTB.EShop.Web.Areas.Eshop.AppServices.Orders
{
	public class OrderAppService : IOrderAppService
	{
		private readonly IOrderCoreService _orderCoreService;
		private readonly ICartCoreService _cartCoreService;

		// CTOR
		public OrderAppService(IOrderCoreService orderCoreService, ICartCoreService cartCoreService)
		{
			_orderCoreService = orderCoreService;
			_cartCoreService = cartCoreService;
		}

		// PUBLIC
		public void CreateOrderFromCart()
		{
			var cart = _cartCoreService.Get();
			_orderCoreService.Create(cart);
		}
	}
}