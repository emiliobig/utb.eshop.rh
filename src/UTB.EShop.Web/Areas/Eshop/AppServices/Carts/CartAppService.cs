﻿using UTB.EShop.Core.CoreServices.Carts;
using UTB.EShop.Core.CoreServices.SelectLists;
using UTB.EShop.Core.Models.Carts;
using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.AppServices.Carts
{
	public class CartAppService : ICartAppService
	{
		private readonly ICartCoreService _cartCoreService;
		private readonly ISelectListCoreService _selectListCoreService;

		// CTOR
		public CartAppService(ICartCoreService cartCoreService, ISelectListCoreService selectListCoreService)
		{
			_cartCoreService = cartCoreService;
			_selectListCoreService = selectListCoreService;
		}

		// PUBLIC
		public CartViewModel GetViewModel(int? currentStep = 0)
		{
			var vm = new CartViewModel
			{
				Cart = _cartCoreService.Get(),
				Deliveries = _selectListCoreService.GetDeliveries(),
				Payments = _selectListCoreService.GetPayments(),
				CurrentStep = currentStep.Value
			};

			return vm;
		}

		public CartItem AddToCart(int productID, int amount)
		{
			return _cartCoreService.AddToCart(productID, amount);
		}

		public void RemoveFromCart(int productID)
		{
			_cartCoreService.RemoveFromCart(productID);
		}

		public void InsertAddresses(CartViewModel model)
		{
			_cartCoreService.InsertBillingAddress(model.Cart.BillingAddress);
			_cartCoreService.InsertShippingAddress(model.Cart.ShippingAddress);
		}

		public void InsertPaymentAndDelivery(CartViewModel model)
		{
			_cartCoreService.InsertPayment(model.Cart.PaymentID);
			_cartCoreService.InsertDelivery(model.Cart.DeliveryID);
		}
	}
}