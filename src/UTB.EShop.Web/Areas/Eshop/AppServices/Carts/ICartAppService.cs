﻿using UTB.EShop.Core.Models.Carts;
using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.AppServices.Carts
{
	public interface ICartAppService
	{
		CartViewModel GetViewModel(int? currentStep = 0);

		CartItem AddToCart(int productID, int amount);

		void RemoveFromCart(int productID);

		void InsertAddresses(CartViewModel model);

		void InsertPaymentAndDelivery(CartViewModel model);
	}
}