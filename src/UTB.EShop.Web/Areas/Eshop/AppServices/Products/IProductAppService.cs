﻿using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.AppServices.Products
{
	public interface IProductAppService
	{
		ProductsViewModel GetAllViewModel();

		ProductViewModel GetViewModel(int productID);
	}
}