﻿using UTB.EShop.Core.CoreServices.Products;
using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.AppServices.Products
{
	public class ProductAppService : IProductAppService
	{
		private readonly IProductCoreService _productCoreService;

		// CTOR
		public ProductAppService(IProductCoreService productCoreService)
		{
			_productCoreService = productCoreService;
		}

		// PUBLIC
		public ProductsViewModel GetAllViewModel()
		{
			return new ProductsViewModel
			{
				Products = _productCoreService.GetAll()
			};
		}

		public ProductViewModel GetViewModel(int productID)
		{
			return new ProductViewModel
			{
				Product = _productCoreService.GetByID(productID)
			};
		}
	}
}