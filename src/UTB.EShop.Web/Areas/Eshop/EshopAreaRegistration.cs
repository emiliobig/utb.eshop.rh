﻿using System.Web.Mvc;

namespace UTB.EShop.Web.Areas.Eshop
{
	public class EshopAreaRegistration : AreaRegistration 
	{
		public override string AreaName 
		{
			get 
			{
				return "Eshop";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context) 
		{
			context.MapRoute(
				"Eshop_default",
				"Eshop/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}