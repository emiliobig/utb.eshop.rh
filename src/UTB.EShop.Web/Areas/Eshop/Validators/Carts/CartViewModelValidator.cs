﻿using FluentValidation;
using UTB.EShop.Web.Areas.Eshop.Models;

namespace UTB.EShop.Web.Areas.Eshop.Validators.Carts
{
	public class CartViewModelValidator : AbstractValidator<CartViewModel>
	{
		public CartViewModelValidator()
		{
			InitRules();
		}

		private void InitRules()
		{
			When(m => m.CurrentStep == 1, () =>
			{
				// BillingAddress
				RuleFor(m => m.Cart.BillingAddress.FirstName)
					.NotNull()
					.WithMessage("Jméno musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Jméno nesmí být prázdné.");

				RuleFor(m => m.Cart.BillingAddress.LastName)
					.NotNull()
					.WithMessage("Příjmení musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Příjmení nesmí být prázdné.");

				RuleFor(m => m.Cart.BillingAddress.StreetNo)
					.NotNull()
					.WithMessage("Číslo popisné musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Číslo popisné nesmí být prázdné.");

				RuleFor(m => m.Cart.BillingAddress.Street)
					.NotNull()
					.WithMessage("Ulice musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Ulice nesmí být prázdné.");

				RuleFor(m => m.Cart.BillingAddress.City)
					.NotNull()
					.WithMessage("Město musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Město nesmí být prázdné.");

				RuleFor(m => m.Cart.BillingAddress.PostalCode)
					.NotNull()
					.WithMessage("PSČ musí být vyplněno.")
					.NotEmpty()
					.WithMessage("PSČ nesmí být prázdné.")
					.MaximumLength(5)
					.WithMessage("PSČ musí mít délku 5 znaků.");

				// ShippingAddress
				RuleFor(m => m.Cart.ShippingAddress.FirstName)
					.NotNull()
					.WithMessage("Jméno musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Jméno nesmí být prázdné.");

				RuleFor(m => m.Cart.ShippingAddress.LastName)
					.NotNull()
					.WithMessage("Příjmení musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Příjmení nesmí být prázdné.");

				RuleFor(m => m.Cart.ShippingAddress.StreetNo)
					.NotNull()
					.WithMessage("Číslo popisné musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Číslo popisné nesmí být prázdné.");

				RuleFor(m => m.Cart.ShippingAddress.Street)
					.NotNull()
					.WithMessage("Ulice musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Ulice nesmí být prázdné.");

				RuleFor(m => m.Cart.ShippingAddress.City)
					.NotNull()
					.WithMessage("Město musí být vyplněno.")
					.NotEmpty()
					.WithMessage("Město nesmí být prázdné.");

				RuleFor(m => m.Cart.ShippingAddress.PostalCode)
					.NotNull()
					.WithMessage("PSČ musí být vyplněno.")
					.NotEmpty()
					.WithMessage("PSČ nesmí být prázdné.")
					.MaximumLength(5)
					.WithMessage("PSČ musí mít délku 5 znaků.");
			});

			When(m => m.CurrentStep == 2, () =>
			{
				RuleFor(m => m.Cart.DeliveryID)
					.NotEqual(0);

				RuleFor(m => m.Cart.PaymentID)
					.NotEqual(0);
			});

			// Template for Custom rule
			//RuleFor(m => m)
			//	.Custom(Test);
		}

		// Template for Custom rule
		//private void Test(CartViewModel model, CustomContext context)
		//{
		//	if (model.CurrentStep == 1)
		//	{
		//		if (model.Cart.BillingAddress.FirstName != "")
		//		{
		//			context.AddFailure(new ValidationFailure("jméno prop", "Error"));
		//		}
		//	}
		//}
	}
}