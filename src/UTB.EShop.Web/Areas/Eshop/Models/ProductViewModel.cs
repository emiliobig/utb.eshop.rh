﻿using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Web.Areas.Eshop.Models
{
	public class ProductViewModel
	{
		public Product Product { get; set; }
	}
}