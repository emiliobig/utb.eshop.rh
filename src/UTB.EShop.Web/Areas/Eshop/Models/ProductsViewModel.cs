﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Web.Areas.Eshop.Models
{
	public class ProductsViewModel
	{
		public IList<Product> Products { get; set; }
	}
}