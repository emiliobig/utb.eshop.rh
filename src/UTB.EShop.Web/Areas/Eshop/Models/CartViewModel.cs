﻿using System.Collections.Generic;
using System.Web.Mvc;
using UTB.EShop.Core.Models.Carts;

namespace UTB.EShop.Web.Areas.Eshop.Models
{
	public class CartViewModel
	{
		public Cart Cart { get; set; }
		public IEnumerable<SelectListItem> Payments { get; set; }
		public IEnumerable<SelectListItem> Deliveries { get; set; }
		public int CurrentStep { get; set; }
	}
}