﻿using FluentValidation;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.Validators.Products
{
	public class ProductViewModelValidator : AbstractValidator<ProductViewModel>
	{
		public ProductViewModelValidator()
		{
			InitRules();
		}

		private void InitRules()
		{
			RuleFor(m => m.Product.Name)
				.NotNull()
				.WithMessage("Jméno musí být vyplněno. [NotNull Rule]")
				.NotEmpty()
				.WithMessage("Jméno nemůže být prázdné. [NotEmpty Rule]")
				.MinimumLength(3)
				.WithMessage("Délka jména musí být větší (rovno) než 3 znaky. [MinimumLength Rule]");

			//RuleFor(m => m.Product.Price)
			//	.NotEqual(0)
			//	.WithMessage("Cena nemůže být nulová. [NotEqual Rule]")
			//	.GreaterThan(0)
			//	.WithMessage("Cena nemůže být záporná. [GreaterThan]");

			// Custom Rule Example - rule (1st implementation - Method group)
			RuleFor(m => m.Product)
			    .Custom(PriceGreaterThan);

			// Custom Rule Example - rule (2nd implementation - Lambda)
			//RuleFor(m => m.Product)
			//    .Custom((product, context) => PriceGreaterThan(product, context)); // ReSharper: "Convert to method group" (tj 1st impl)

		}

		// Custom Rule Example - implementation
		private void PriceGreaterThan(Core.Models.Products.Product product, FluentValidation.Validators.CustomContext context)
		{
			if (product.Price <= 0)
			{
				context.AddFailure(
					new FluentValidation.Results.ValidationFailure(
						$"{nameof(Core.Models.Products.Product)}.{nameof(Core.Models.Products.Product.Price)}"
						, "Cena nemůže být záporná ani nulová. [Custom PriceGreaterThan Rule]"));
			}
		}
	}
}