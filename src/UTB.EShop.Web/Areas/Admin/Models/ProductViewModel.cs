﻿using System.Collections.Generic;
using System.Web;
using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Web.Areas.Admin.Models
{
	public class ProductViewModel
	{
		public Product Product { get; set; }
		public IList<Category> Categories { get; set; }
		public HttpPostedFileBase Image { get; set; }
	}
}