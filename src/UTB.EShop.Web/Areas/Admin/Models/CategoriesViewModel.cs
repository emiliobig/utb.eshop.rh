﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Categories;

namespace UTB.EShop.Web.Areas.Admin.Models
{
	public class CategoriesViewModel
	{
		public IList<Category> Categories { get; set; }
	}
}