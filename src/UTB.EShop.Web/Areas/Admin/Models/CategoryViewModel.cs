﻿using UTB.EShop.Core.Models.Categories;

namespace UTB.EShop.Web.Areas.Admin.Models
{
	public class CategoryViewModel
	{
		public Category Category { get; set; }
	}
}