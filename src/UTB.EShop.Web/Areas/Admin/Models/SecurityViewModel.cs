﻿namespace UTB.EShop.Web.Areas.Admin.Models
{
	public class SecurityViewModel
	{
		public string Password { get; set; }

		public string Login { get; set; }

		public string ErrorMessage { get; set; }
	}
}