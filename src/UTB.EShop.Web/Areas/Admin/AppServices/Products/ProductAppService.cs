﻿using System.Linq;
using UTB.EShop.Core.CoreServices.Categories;
using UTB.EShop.Core.CoreServices.Files;
using UTB.EShop.Core.CoreServices.Products;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.AppServices.Products
{
	public class ProductAppService : IProductAppService
	{
		private readonly ICategoryCoreService _categoryCoreService;
		private readonly IProductCoreService _productCoreService;
		private readonly IFileResolver _fileResolver;

		// CTOR
		public ProductAppService(ICategoryCoreService categoryCoreService,
								 IProductCoreService productCoreService,
								 IFileResolver fileResolver)
		{
			_categoryCoreService = categoryCoreService;
			_productCoreService = productCoreService;
			_fileResolver = fileResolver;
		}

		// PUBLIC
		public ProductsViewModel GetAllViewModel()
		{
			return new ProductsViewModel
			{
				Products = _productCoreService.GetAll()
			};
		}

		public ProductViewModel GetViewModel(int? productID)
		{
			if (productID.HasValue)
			{
				var categories = _categoryCoreService.GetAll();
				var assignedCategories = _productCoreService.AssignedProductCategories(productID.Value);

				foreach (var category in categories)
				{
					category.IsSelected = assignedCategories.Any(ac => ac.CategoryID == category.CategoryID);
				}

				return new ProductViewModel
				{
					Product = _productCoreService.GetByID(productID.Value),
					Categories = categories
				};
			}

			return new ProductViewModel();
		}

		public void Insert(ProductViewModel model)
		{
			if (model.Image != null)
			{
				model.Product.ImageUrl = _fileResolver.SaveImageAndGetImageUrl(model.Image);
			}

			_productCoreService.Insert(model.Product);
		}

		public void Update(ProductViewModel model)
		{
			if (model.Image != null)
			{
				model.Product.ImageUrl = _fileResolver.SaveImageAndGetImageUrl(model.Image);
			}

			_productCoreService.Update(model.Product);
		}

		public void Delete(int productID)
		{
			_productCoreService.Delete(productID);
		}

		public void InsertOrRemoveProductIntoOrFromCategories(ProductViewModel model)
		{
			foreach (var category in model.Categories)
			{
				if (category.IsSelected)
				{
					_categoryCoreService.InsertProductIntoCategory(model.Product.ProductID, category.CategoryID);
				}
				else
				{
					_categoryCoreService.RemoveProductFromCategory(model.Product.ProductID, category.CategoryID);
				}
			}
		}
	}
}