﻿using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.AppServices.Products
{
	public interface IProductAppService
	{
		ProductsViewModel GetAllViewModel();

		ProductViewModel GetViewModel(int? productID);

		void Insert(ProductViewModel model);
		
		void Update(ProductViewModel model);

		void Delete(int productID);

		// Passes model property into product service for further actions with product categories
		void InsertOrRemoveProductIntoOrFromCategories(ProductViewModel model);
	}
}