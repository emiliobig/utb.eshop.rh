﻿using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.AppServices.Categories
{
	public interface ICategoryAppService
	{
		CategoriesViewModel GetAllViewModel();

		CategoryViewModel GetViewModel(int? categoryID);

		void Insert(CategoryViewModel model);

		void Update(CategoryViewModel model);

		void Delete(int categoryID);
	}
}