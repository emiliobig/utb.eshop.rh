﻿using UTB.EShop.Core.CoreServices.Categories;
using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.AppServices.Categories
{
	public class CategoryAppService : ICategoryAppService
	{
		private readonly ICategoryCoreService _categoryCoreService;

		// CTOR
		public CategoryAppService(ICategoryCoreService categoryCoreService)
		{
			_categoryCoreService = categoryCoreService;
		}

		// PUBLIC
		public CategoriesViewModel GetAllViewModel()
		{
			return new CategoriesViewModel
			{
				Categories = _categoryCoreService.GetAll()
			};
		}

		public CategoryViewModel GetViewModel(int? categoryID)
		{
			if (categoryID.HasValue)
			{
				return new CategoryViewModel
				{
					Category = _categoryCoreService.GetByID(categoryID.Value)
				};
			}

			return new CategoryViewModel
			{
				Category = new Category()
			};
		}

		public void Insert(CategoryViewModel model)
		{
			_categoryCoreService.Insert(model.Category);
		}

		public void Update(CategoryViewModel model)
		{
			_categoryCoreService.Update(model.Category);
		}

		public void Delete(int categoryID)
		{
			_categoryCoreService.Delete(categoryID);
		}
	}
}