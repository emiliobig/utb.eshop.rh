﻿using System.Web.Mvc;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.Controllers
{
	public class SecurityController : Controller
	{
		private readonly IFormAuthentication _formAuthentication;

		// CTOR
		public SecurityController(IFormAuthentication formAuthentication)
		{
			_formAuthentication = formAuthentication;
		}

		// GET: Admin/Security/Login
		public ActionResult Login()
		{
			var vm = new SecurityViewModel();
			return View(vm);
		}

		// POST: Admin/Security/Login
		[HttpPost]
		public ActionResult Login(SecurityViewModel model)
		{
			if (_formAuthentication.Authenticate(model.Login, model.Password))
			{
				return RedirectToAction("Index", "Products");
			}
			else
			{
				model.ErrorMessage = "Přihlášení se nezdařilo";
			}

			return View(model);
		}

		// GET: Admin/Security/Logout
		public RedirectToRouteResult Logout()
		{
			_formAuthentication.SignOut();

			return RedirectToAction("Login");
		}
	}
}