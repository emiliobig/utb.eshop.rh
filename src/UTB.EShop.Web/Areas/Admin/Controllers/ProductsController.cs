﻿using System.Web.Mvc;
using UTB.EShop.Core.Models.Users;
using UTB.EShop.Core.Mvc.Permissions;
using UTB.EShop.Web.Areas.Admin.AppServices.Products;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.Controllers
{
	[Authorize]
	public class ProductsController : Controller
	{
		private readonly IProductAppService _productAppService;

		// CTOR
		public ProductsController(IProductAppService productAppService)
		{
			_productAppService = productAppService;
		}

		// GET: Admin/Products
		[HttpGet]
		public ViewResult Index()
		{
			var vm = _productAppService.GetAllViewModel();

			return View(vm);
		}

		[HttpGet]
		[HasAccessRole(Roles.Admin)]
		public ViewResult Create()
		{
			var vm = _productAppService.GetViewModel(null);

			return View();
		}

		[HttpPost]
		public ActionResult Create(ProductViewModel vm)
		{
			if (!ModelState.IsValid)
			{
				return View(vm);
			}

			_productAppService.Insert(vm);

			return RedirectToAction("Index");
		}

		[HttpGet]
		[HasAccessRole(Roles.Manager)]
		public ActionResult Edit(int productID)
		{
			var vm = _productAppService.GetViewModel(productID);

			if (vm.Product == null)
			{
				return RedirectToAction("Error404", "Error");
			}

			return View(vm);
		}

		[HttpPost]
		public RedirectToRouteResult Edit(ProductViewModel vm)
		{
			_productAppService.Update(vm);

			return RedirectToAction("Index");
		}

		[HttpGet]
		public RedirectToRouteResult Delete(int productID)
		{
			_productAppService.Delete(productID);

			return RedirectToAction("Index");
		}

		[HttpPost]
		public RedirectToRouteResult InsertOrRemoveCategories(ProductViewModel vm)
		{
			_productAppService.InsertOrRemoveProductIntoOrFromCategories(vm);

			return RedirectToAction("Index");
		}
	}
}