﻿using System.Web.Mvc;
using UTB.EShop.Web.Areas.Admin.AppServices.Categories;
using UTB.EShop.Web.Areas.Admin.Models;

namespace UTB.EShop.Web.Areas.Admin.Controllers
{
	[Authorize]
	public class CategoriesController : Controller
    {
	    private readonly ICategoryAppService _categoryAppService;

	    public CategoriesController(ICategoryAppService categoryAppService)
	    {
		    _categoryAppService = categoryAppService;
	    }

		// GET: Admin/Categories
		[HttpGet]
		public ActionResult Index()
	    {
		    var vm = _categoryAppService.GetAllViewModel();

		    return View(vm);
	    }

	    [HttpGet]
	    public ViewResult Create()
	    {
		    var vm = _categoryAppService.GetViewModel(null);

		    return View(vm);
	    }

	    [HttpPost]
	    public RedirectToRouteResult Create(CategoryViewModel vm)
	    {
		    _categoryAppService.Insert(vm);

		    return RedirectToAction("Index");
	    }

		[HttpGet]
		public ViewResult Edit(int categoryID)
	    {
		    var vm = _categoryAppService.GetViewModel(categoryID);

		    return View(vm);
	    }

	    [HttpPost]
	    public RedirectToRouteResult Edit(CategoryViewModel vm)
	    {
		    _categoryAppService.Update(vm);

		    return RedirectToAction("Index");
	    }

		[HttpGet]
	    public RedirectToRouteResult Delete(int categoryID)
	    {
		    _categoryAppService.Delete(categoryID);

		    return RedirectToAction("Index");
	    }
	}
}