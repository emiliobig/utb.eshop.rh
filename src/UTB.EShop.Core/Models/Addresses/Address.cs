﻿using System;

namespace UTB.EShop.Core.Models.Addresses
{
	public class Address : ICloneable
	{
		public int AddressID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Street { get; set; }
		public string StreetNo { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }

		public object Clone()
		{
			return new Address
			{
				FirstName = FirstName,
				LastName = LastName,
				Street = Street,
				StreetNo = StreetNo,
				City = City,
				PostalCode = PostalCode
			};
		}
	}
}