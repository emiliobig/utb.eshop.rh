﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Addresses;

namespace UTB.EShop.Core.Models.Carts
{
	public class Cart
	{
		public Cart()
		{
			CartItems = new List<CartItem>();
		}

		public int CartID { get; set; }
		public string UserTrackingCode { get; set; }
		public double TotalPrice { get; set; }
		public int? BillingAddressID { get; set; }
		public int? ShippingAddressID { get; set; }
		public int PaymentID { get; set; }
		public int DeliveryID { get; set; }

		public IList<CartItem> CartItems { get; set; }
		public Address BillingAddress { get; set; }
		public Address ShippingAddress { get; set; }
	}
}