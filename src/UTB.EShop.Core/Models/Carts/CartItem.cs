﻿using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.Models.Carts
{
	public class CartItem
	{
		public int CartItemID { get; set; }
		public int CartID { get; set; }
		public int ProductID { get; set; }
		public int Amount { get; set; }

		public Cart Cart { get; set; }
		public Product Product { get; set; }
	}
}