﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.CategoriesProducts;

namespace UTB.EShop.Core.Models.Categories
{
	public class Category
	{
		public int CategoryID { get; set; }
		public string Name { get; set; }

		public bool IsSelected { get; set; }

		public IList<CategoryProduct> Products { get; set; }
	}
}