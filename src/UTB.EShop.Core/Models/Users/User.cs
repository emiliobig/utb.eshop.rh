﻿namespace UTB.EShop.Core.Models.Users
{
	public class User
	{
		public int UserID { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string PasswordSalt { get; set; }

		public Roles Roles { get; set; }
	}
}