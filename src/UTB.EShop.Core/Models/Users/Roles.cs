﻿using System;

namespace UTB.EShop.Core.Models.Users
{
	[Flags]
	public enum Roles
	{
		None = 0,
		User = 1,
		Manager = 2,
		Admin = 4,
		All = Admin | Manager | User
	}
}