﻿namespace UTB.EShop.Core.Models.Shipping
{
	public class Delivery
	{
		public int DeliveryID { get; set; }
		public string Name { get; set; }
	}
}