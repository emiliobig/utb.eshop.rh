﻿namespace UTB.EShop.Core.Models.Shipping
{
	public class Payment
	{
		public int PaymentID { get; set; }
		public string Name { get; set; }
	}
}