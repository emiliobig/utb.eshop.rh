﻿using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.Models.Orders
{
	public class OrderItem
	{
		public int OrderItemID { get; set; }
		public int OrderID { get; set; }
		public int ProductID { get; set; }

		public Order Order { get; set; }
		public Product Product { get; set; }
	}
}