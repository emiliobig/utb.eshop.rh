﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Addresses;

namespace UTB.EShop.Core.Models.Orders
{
	public class Order
	{
		public Order()
		{
			OrderItems = new List<OrderItem>();
		}

		public int OrderID { get; set; }
		public string OrderNumber { get; set; }
		public double TotalPrice { get; set; }
		public int? BillingAddressID { get; set; }
		public int? ShippingAddressID { get; set; }
		public int PaymentID { get; set; }
		public int DeliveryID { get; set; }

		public IList<OrderItem> OrderItems { get; set; }
		public Address BillingAddress { get; set; }
		public Address ShippingAddress { get; set; }
	}
}