﻿using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.Models.CategoriesProducts
{
	public class CategoryProduct
	{
		public int CategoryID { get; set; }
		public int ProductID { get; set; }

		public Category Category { get; set; }
		public Product Product { get; set; }
	}
}