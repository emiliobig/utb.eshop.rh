﻿using System;
using System.Collections.Generic;
using UTB.EShop.Core.Models.CategoriesProducts;

namespace UTB.EShop.Core.Models.Products
{
	public class Product
	{
		public int ProductID { get; set; }
		public string Name { get; set; }
		public double Price { get; set; }
		public string ImageUrl { get; set; }
		public DateTime DateCreated { get; set; }
		public int CreatedBy { get; set; }
		public DateTime DateUpdated { get; set; }
		public int UpdatedBy { get; set; }

		public IList<CategoryProduct> Categories { get; set; }
	}
}