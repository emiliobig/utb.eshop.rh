namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoriesProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Web.CategoriesProducts",
                c => new
                    {
                        CategoryID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CategoryID, t.ProductID })
                .ForeignKey("Web.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("Web.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.ProductID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Web.CategoriesProducts", "ProductID", "Web.Products");
            DropForeignKey("Web.CategoriesProducts", "CategoryID", "Web.Categories");
            DropIndex("Web.CategoriesProducts", new[] { "ProductID" });
            DropIndex("Web.CategoriesProducts", new[] { "CategoryID" });
            DropTable("Web.CategoriesProducts");
        }
    }
}
