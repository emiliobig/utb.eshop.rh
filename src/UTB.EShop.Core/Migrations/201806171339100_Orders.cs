namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Orders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Web.OrderItems",
                c => new
                    {
                        OrderItemID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderItemID)
                .ForeignKey("Web.Orders", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("Web.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "Web.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        OrderNumber = c.String(),
                        TotalPrice = c.Double(nullable: false),
                        BillingAddressID = c.Int(),
                        ShippingAddressID = c.Int(),
                        PaymentID = c.Int(nullable: false),
                        DeliveryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("Web.Addresses", t => t.BillingAddressID)
                .ForeignKey("Web.Addresses", t => t.ShippingAddressID)
                .Index(t => t.BillingAddressID)
                .Index(t => t.ShippingAddressID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Web.OrderItems", "ProductID", "Web.Products");
            DropForeignKey("Web.Orders", "ShippingAddressID", "Web.Addresses");
            DropForeignKey("Web.OrderItems", "OrderID", "Web.Orders");
            DropForeignKey("Web.Orders", "BillingAddressID", "Web.Addresses");
            DropIndex("Web.Orders", new[] { "ShippingAddressID" });
            DropIndex("Web.Orders", new[] { "BillingAddressID" });
            DropIndex("Web.OrderItems", new[] { "ProductID" });
            DropIndex("Web.OrderItems", new[] { "OrderID" });
            DropTable("Web.Orders");
            DropTable("Web.OrderItems");
        }
    }
}
