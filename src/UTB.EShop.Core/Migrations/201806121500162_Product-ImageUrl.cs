namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductImageUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("Web.Products", "ImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Web.Products", "ImageUrl");
        }
    }
}
