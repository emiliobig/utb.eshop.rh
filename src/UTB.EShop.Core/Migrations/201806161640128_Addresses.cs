namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addresses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Web.Addresses",
                c => new
                    {
                        AddressID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Street = c.String(),
                        StreetNo = c.String(),
                        City = c.String(),
                        PostalCode = c.String(),
                    })
                .PrimaryKey(t => t.AddressID);
            
            AddColumn("Web.Carts", "BillingAddressID", c => c.Int());
            AddColumn("Web.Carts", "ShippingAddressID", c => c.Int());
            AddColumn("Web.Carts", "PaymentID", c => c.Int(nullable: false));
            AddColumn("Web.Carts", "DeliveryID", c => c.Int(nullable: false));
            CreateIndex("Web.Carts", "BillingAddressID");
            CreateIndex("Web.Carts", "ShippingAddressID");
            AddForeignKey("Web.Carts", "BillingAddressID", "Web.Addresses", "AddressID");
            AddForeignKey("Web.Carts", "ShippingAddressID", "Web.Addresses", "AddressID");
        }
        
        public override void Down()
        {
            DropForeignKey("Web.Carts", "ShippingAddressID", "Web.Addresses");
            DropForeignKey("Web.Carts", "BillingAddressID", "Web.Addresses");
            DropIndex("Web.Carts", new[] { "ShippingAddressID" });
            DropIndex("Web.Carts", new[] { "BillingAddressID" });
            DropColumn("Web.Carts", "DeliveryID");
            DropColumn("Web.Carts", "PaymentID");
            DropColumn("Web.Carts", "ShippingAddressID");
            DropColumn("Web.Carts", "BillingAddressID");
            DropTable("Web.Addresses");
        }
    }
}
