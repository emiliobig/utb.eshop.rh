namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Carts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Web.CartItems",
                c => new
                    {
                        CartItemID = c.Int(nullable: false, identity: true),
                        CartID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CartItemID)
                .ForeignKey("Web.Carts", t => t.CartID, cascadeDelete: true)
                .ForeignKey("Web.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.CartID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "Web.Carts",
                c => new
                    {
                        CartID = c.Int(nullable: false, identity: true),
                        UserTrackingCode = c.String(),
                        TotalPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.CartID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Web.CartItems", "ProductID", "Web.Products");
            DropForeignKey("Web.CartItems", "CartID", "Web.Carts");
            DropIndex("Web.CartItems", new[] { "ProductID" });
            DropIndex("Web.CartItems", new[] { "CartID" });
            DropTable("Web.Carts");
            DropTable("Web.CartItems");
        }
    }
}
