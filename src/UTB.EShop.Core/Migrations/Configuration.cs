using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;

namespace UTB.EShop.Core.Migrations
{
	internal sealed class Configuration : DbMigrationsConfiguration<UTB.EShop.Core.EntityFramework.DataContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(UTB.EShop.Core.EntityFramework.DataContext context)
		{
			//System.Diagnostics.Debugger.Launch();

			context.Users.AddOrUpdate(
				new Models.Users.User { UserID = 1, Login = "adam@eshop.cz", Password = "heslosalt", PasswordSalt = "salt", FirstName = "Adam", LastName = "Prvn�", Roles = Models.Users.Roles.User },
				new Models.Users.User { UserID = 2, Login = "bob@eshop.cz", Password = "heslosalt", PasswordSalt = "salt", FirstName = "Bob", LastName = "Druh�", Roles = Models.Users.Roles.Manager },
				new Models.Users.User { UserID = 3, Login = "cecil@eshop.cz", Password = "heslosalt", PasswordSalt = "salt", FirstName = "Cecil", LastName = "T�et�", Roles = Models.Users.Roles.Admin },
				new Models.Users.User { UserID = 4, Login = "david@eshop.cz", Password = "heslosalt", PasswordSalt = "salt", FirstName = "David", LastName = "�tvrt�", Roles = Models.Users.Roles.All }
			);
			context.SaveChanges();

			context.Products.AddOrUpdate(
				new Models.Products.Product { ProductID = 1, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, Name = "Triko", Price = 351.00 },
				new Models.Products.Product { ProductID = 2, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, Name = "Mikina", Price = 521.28 },
				new Models.Products.Product { ProductID = 3, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, Name = "Televize", Price = 9999.90 }
			);
			context.SaveChanges();

			context.Categories.AddOrUpdate(
				new Models.Categories.Category { CategoryID = 1, Name = "Oble�en�" },
				new Models.Categories.Category { CategoryID = 2, Name = "Elektronika" }
			);
			context.SaveChanges();

			context.CategoriesProducts.AddOrUpdate(
				new Models.CategoriesProducts.CategoryProduct { CategoryID = 1, ProductID = 1 },
				new Models.CategoriesProducts.CategoryProduct { CategoryID = 1, ProductID = 2 },
				new Models.CategoriesProducts.CategoryProduct { CategoryID = 2, ProductID = 3 }
			);
			context.SaveChanges();

			context.Database.ExecuteSqlCommand(
				$@"IF NOT EXISTS (SELECT 1 FROM sys.sequences WHERE object_id = OBJECT_ID('[{Constants.Schemas.Web}].[{Constants.Sequences.OrderNumberSequence}]'))
                BEGIN
                    CREATE SEQUENCE [{Constants.Schemas.Web}].[{Constants.Sequences.OrderNumberSequence}];
                END");
		}
	}
}