namespace UTB.EShop.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Roles : DbMigration
    {
        public override void Up()
        {
            AddColumn("App.Users", "Roles", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("App.Users", "Roles");
        }
    }
}
