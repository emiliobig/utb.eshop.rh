﻿namespace UTB.EShop.Core.Constants
{
	public class Tables
	{
		public const string Users = "Users";
		public const string Products = "Products";
		public const string Categories = "Categories";
		public const string CategoriesProducts = "CategoriesProducts";
		public const string Carts = "Carts";
		public const string CartItems = "CartItems";
		public const string Addresses = "Addresses";
		public const string Orders = "Orders";
		public const string OrderItems = "OrderItems";
	}
}