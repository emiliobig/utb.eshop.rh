﻿using System;
using System.Linq;
using System.Web;

namespace UTB.EShop.Core.CoreServices.Cookies
{
	public class CookieCoreService : ICookieCoreService
	{
		// PUBLIC
		public void Clear()
		{
			var cookieKeys = HttpContext.Current.Request.Cookies.AllKeys;

			foreach (var cookieKey in cookieKeys)
			{
				var cookie = GetCookie(cookieKey);
				if (cookie != null)
				{
					cookie.Expires = DateTime.Now.AddDays(-1);
				}
			}
		}

		public void Clear(string key)
		{
			var cookie = GetCookie(key);
			if (cookie != null)
			{
				cookie.Expires = DateTime.Now.AddDays(-1);
				SetOrAdd(cookie);
			}
		}

		public void Save(string key, string value)
		{
			var cookie = new HttpCookie(key, value);
			SetOrAdd(cookie);
		}

		public void Save(string key, string value, DateTime expiration)
		{
			var cookie = new HttpCookie(key, value) { Expires = expiration };
			SetOrAdd(cookie);
		}

		public HttpCookie GetCookie(string key)
		{
			return HttpContext.Current.Request.Cookies.Get(key);
		}

		public string GetValue(string key)
		{
			return GetCookie(key)?.Value;
		}

		private static void SetOrAdd(HttpCookie cookie)
		{
			if (HttpContext.Current.Request.Cookies.AllKeys.Contains(cookie.Name))
			{
				HttpContext.Current.Response.Cookies.Set(cookie);
			}
			else
			{
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}
	}
}