﻿using System;
using System.Web;

namespace UTB.EShop.Core.CoreServices.Cookies
{
	public interface ICookieCoreService
	{
		void Clear();

		void Clear(string key);

		void Save(string key, string value);

		void Save(string key, string value, DateTime expiration);

		HttpCookie GetCookie(string key);

		string GetValue(string key);
	}
}