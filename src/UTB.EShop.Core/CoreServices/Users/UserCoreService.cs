﻿using System.Linq;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.CoreServices.Users
{
	public class UserCoreService : IUserCoreService
	{
		private readonly DataContext _dbContext;

		// CTOR
		public UserCoreService(DataContext dbContext)
		{
			_dbContext = dbContext;
		}

		// PUBLIC
		public User GetByID(int id)
		{
			return _dbContext
				.Users
				.SingleOrDefault(e => e.UserID == id);
		}

		public User GetByLogin(string login)
		{
			return _dbContext
				.Users
				.SingleOrDefault(e => e.Login == login);
		}
	}
}