﻿using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.CoreServices.Users
{
	public interface IUserCoreService
	{
		User GetByID(int id);

		User GetByLogin(string login);
	}
}