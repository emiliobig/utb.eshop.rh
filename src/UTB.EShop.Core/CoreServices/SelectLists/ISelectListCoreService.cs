﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace UTB.EShop.Core.CoreServices.SelectLists
{
	public interface ISelectListCoreService
	{
		IEnumerable<SelectListItem> GetDeliveries();
		IEnumerable<SelectListItem> GetPayments();
	}
}