﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UTB.EShop.Core.CoreServices.Shipping;

namespace UTB.EShop.Core.CoreServices.SelectLists
{
	public class SelectListCoreService : ISelectListCoreService
	{
		private readonly IDeliveryCoreService _deliveryCoreService;
		private readonly IPaymentCoreService _paymentCoreService;

		public SelectListCoreService(IDeliveryCoreService deliveryCoreService, IPaymentCoreService paymentCoreService)
		{
			_deliveryCoreService = deliveryCoreService;
			_paymentCoreService = paymentCoreService;
		}

		public IEnumerable<SelectListItem> GetDeliveries()
		{
			return _deliveryCoreService.GetDeliveries()
				.Select(d =>
					new SelectListItem
					{
						Text = d.Name,
						Value = d.DeliveryID.ToString()
					})
				.ToList();
		}

		public IEnumerable<SelectListItem> GetPayments()
		{
			return _paymentCoreService.GetPayments()
				.Select(d =>
					new SelectListItem
					{
						Text = d.Name,
						Value = d.PaymentID.ToString()
					})
				.ToList();
		}
	}
}