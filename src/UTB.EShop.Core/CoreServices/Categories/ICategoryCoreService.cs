﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Categories;

namespace UTB.EShop.Core.CoreServices.Categories
{
	public interface ICategoryCoreService
	{
		IList<Category> GetAll();

		Category GetByID(int categoryID);

		void Insert(Category category);

		void Update(Category category);

		void Delete(int categoryID);

		void InsertProductIntoCategory(int productID, int categoryID);

		void RemoveProductFromCategory(int productID, int categoryID);
	}
}
