﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Core.Models.CategoriesProducts;

namespace UTB.EShop.Core.CoreServices.Categories
{
	public class CategoryCoreService : ICategoryCoreService
	{
		private readonly DataContext _dbContext;

		// CTOR
		public CategoryCoreService(DataContext dbContext)
		{
			_dbContext = dbContext;
		}

		// PUBLIC
		public IList<Category> GetAll()
		{
			return _dbContext
				.Categories
				.AsNoTracking()
				.ToList();
		}

		public Category GetByID(int categoryID)
		{
			return _dbContext.Categories.Find(categoryID);
		}

		public void Insert(Category category)
		{
			_dbContext.Categories.Add(category);
			_dbContext.SaveChanges();
		}

		public void Update(Category newCategory)
		{
			var category = GetByID(newCategory.CategoryID);

			category.Name = newCategory.Name;

			_dbContext.Entry(category).State = EntityState.Modified;
			_dbContext.SaveChanges();
		}

		public void Delete(int categoryID)
		{
			var category = GetByID(categoryID);
			_dbContext.Categories.Remove(category);
			_dbContext.SaveChanges();
		}

		public void InsertProductIntoCategory(int productID, int categoryID)
		{
			var isProductInCategory = IsProductInCategory(productID, categoryID);

			if (!isProductInCategory)
			{
				_dbContext.CategoriesProducts.Add(new CategoryProduct { ProductID = productID, CategoryID = categoryID });
				_dbContext.SaveChanges();
			}
		}

		public void RemoveProductFromCategory(int productID, int categoryID)
		{
			var isProductInCategory = IsProductInCategory(productID, categoryID);

			if (isProductInCategory)
			{
				var product = GetSingleCategoryProductBy(pc => pc.ProductID == productID && pc.CategoryID == categoryID);

				_dbContext.CategoriesProducts.Remove(product);
				_dbContext.SaveChanges();
			}
		}

		// PRIVATE
		private bool IsProductInCategory(int productID, int categoryID)
		{
			return GetSingleCategoryProductBy(pc => pc.ProductID == productID && pc.CategoryID == categoryID) != default(CategoryProduct);
		}

		private CategoryProduct GetSingleCategoryProductBy(Func<CategoryProduct, bool> predicate)
		{
			return _dbContext.CategoriesProducts.SingleOrDefault(predicate);
		}
	}
}