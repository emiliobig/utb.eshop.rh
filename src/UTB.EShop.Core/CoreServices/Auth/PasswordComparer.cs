﻿namespace UTB.EShop.Core.CoreServices.Auth
{
	public class PasswordComparer : IPasswordComparer
	{
		public bool Matches(string testedPassword, string password, string passwordSalt)
		{
			return testedPassword.Equals(password.Replace(passwordSalt, ""));
		}
	}
}
