﻿using System.Web;
using System.Web.Security;
using UTB.EShop.Core.CoreServices.Users;

namespace UTB.EShop.Core.CoreServices.Auth
{
	public class FormAuthentication : IFormAuthentication
	{
		private readonly IPasswordComparer _passwordComparer;
		private readonly ISessionStateHandler _sessionStateHandler;
		private readonly IUserCoreService _userService;

		// CTOR
		public FormAuthentication(IPasswordComparer passwordComparer,
			ISessionStateHandler sessionStateHandler,
			IUserCoreService userService)
		{
			_passwordComparer = passwordComparer;
			_sessionStateHandler = sessionStateHandler;
			_userService = userService;
		}

		public bool Authenticate(string login, string password)
		{
			var user = _userService.GetByLogin(login);

			if (user == null)
				return false;

			if (!_passwordComparer.Matches(password, user.Password, user.PasswordSalt))
				return false;

			FormsAuthentication.SetAuthCookie(user.UserID.ToString(), true);
			_sessionStateHandler.SetCurrentUser(user);

			return true;
		}

		public bool IsAuthenticated()
		{
			var context = HttpContext.Current;

			return context != null
				   && context.Session != null
				   && context.User != null
				   && context.User.Identity.IsAuthenticated;
		}

		public void SignOut()
		{
			FormsAuthentication.SignOut();
			HttpContext.Current.Session.Clear();
			HttpContext.Current.Session.Abandon();
		}
	}
}