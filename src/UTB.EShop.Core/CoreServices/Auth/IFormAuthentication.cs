﻿namespace UTB.EShop.Core.CoreServices.Auth
{
	public interface IFormAuthentication
	{
		// Attempts to authenticate user. If authentication is sucessful then user is stored into session
		bool Authenticate(string login, string password);

		// Is current user authenticated?
		bool IsAuthenticated();

		// Signouts current user and clears session
		void SignOut();
	}
}