﻿using System.Web;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.CoreServices.Auth
{
	public class SessionStateHandler : ISessionStateHandler
	{
		private const string SessionKey = "UtbEshop.UserSessionKey";

		public User GetCurrentUser()
		{
			var sessionItem = HttpContext.Current.Session[SessionKey];

			if (sessionItem == null)
				return null;

			return (User)sessionItem;
		}

		public void SetCurrentUser(User user)
		{
			HttpContext.Current.Session[SessionKey] = user;
		}
	}
}
