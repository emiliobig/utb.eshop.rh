﻿using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.CoreServices.Auth
{
	public interface ISessionStateHandler
	{
		// Gets current user that is stored in session. Returns null if there is no user stored
		User GetCurrentUser();

		// Stores authenticated user into session
		void SetCurrentUser(User user);
	}
}
