﻿namespace UTB.EShop.Core.CoreServices.Auth
{
	public interface IPasswordComparer
	{
		// Tests whether passed testedPassword matches hashed password of tested user
		bool Matches(string testedPassword, string password, string passwordSalt);
	}
}