﻿using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.CoreServices.Orders
{
	public interface IOrderNumberGenerator
	{
		int GetNextOrderNumber();
	}
}