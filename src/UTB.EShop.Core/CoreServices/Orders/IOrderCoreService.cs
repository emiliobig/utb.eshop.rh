﻿using UTB.EShop.Core.Models.Carts;
using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.CoreServices.Orders
{
	public interface IOrderCoreService
	{
		Order Create(Cart cart);
	}
}