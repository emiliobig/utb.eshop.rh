﻿using UTB.EShop.Core.CoreServices.Cookies;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Addresses;
using UTB.EShop.Core.Models.Carts;
using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.CoreServices.Orders
{
	public class OrderCoreService : IOrderCoreService
	{
		private readonly DataContext _dataContext;
		private readonly IOrderNumberGenerator _orderNumberGenerator;
		private readonly ICookieCoreService _cookieCoreService;

		public OrderCoreService(DataContext dataContext, IOrderNumberGenerator orderNumberGenerator, ICookieCoreService cookieCoreService)
		{
			_dataContext = dataContext;
			_orderNumberGenerator = orderNumberGenerator;
			_cookieCoreService = cookieCoreService;
		}

		public Order Create(Cart cart)
		{
			var order = new Order
			{
				TotalPrice = cart.TotalPrice,
				DeliveryID = cart.DeliveryID,
				PaymentID = cart.PaymentID,
				BillingAddress = cart.BillingAddress.Clone() as Address,
				ShippingAddress = cart.ShippingAddress.Clone() as Address
			};

			// TODO ze sessionHandleru vytáhnout accountID (prdime na to, neni cas)
			// cart.AccountID = accountID;
			// cart.Account = account;
			// TODO pokud není accountID
			// založení uživatele (AccountType.UnregisteredAccount)
			// cart.AccountID = accountID;
			// cart.Account = account;

			order.OrderNumber = _orderNumberGenerator.GetNextOrderNumber().ToString();

			foreach (var cartItem in cart.CartItems)
			{
				var orderItem = new OrderItem
				{
					ProductID = cartItem.ProductID
				};

				order.OrderItems.Add(orderItem);
			}

			_dataContext.Orders.Add(order);
			_dataContext.Carts.Remove(cart);
			_dataContext.SaveChanges();

			_cookieCoreService.Clear(Constants.Cookies.CartTrackingCode);

			return order;
		}
	}
}