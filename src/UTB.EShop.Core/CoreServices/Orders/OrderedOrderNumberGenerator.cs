﻿using System;
using System.Data.SqlClient;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.CoreServices.Orders
{
	public class OrderedOrderNumberGenerator : IOrderNumberGenerator
	{
		public int GetNextOrderNumber()
		{
			using (var dbConnection = new SqlConnection(DataContext.ConnectionString))
			{
				var command = new SqlCommand("SELECT NEXT VALUE FOR [Web].[OrderNumberSequence]", dbConnection);

				try
				{
					dbConnection.Open();

					return Convert.ToInt32(command.ExecuteScalar());
				}
				catch
				{
					// ignorovat, vždy bude hodnota z db
					return -1;
				}
			}
		}
	}
}