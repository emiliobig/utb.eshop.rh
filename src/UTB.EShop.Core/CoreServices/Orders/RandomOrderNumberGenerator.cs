﻿using System;

namespace UTB.EShop.Core.CoreServices.Orders
{
	public class RandomOrderNumberGenerator : IOrderNumberGenerator
	{
		public int GetNextOrderNumber()
		{
			var randomNumber = new Random(int.MaxValue).Next();

			return randomNumber;
		}
	}
}