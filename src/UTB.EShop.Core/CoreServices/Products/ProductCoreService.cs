﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.CategoriesProducts;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.CoreServices.Products
{
	public class ProductCoreService : IProductCoreService
	{
		private readonly DataContext _dbContext;
		private readonly ISessionStateHandler _session;

		// CTOR
		public ProductCoreService(DataContext dbContext, ISessionStateHandler sessionStateHandler)
		{
			_dbContext = dbContext;
			_session = sessionStateHandler;
		}

		// PUBLIC
		public IList<Product> GetAll()
		{
			return _dbContext
				.Products
				.AsNoTracking()											// speed optimalization
				.ToList();
		}

		public Product GetByID(int productID)
		{
			return _dbContext.Products.Find(productID);					// Find() - search in DBSet by Primary Key
		}

		public void Insert(Product product)
		{
			// product default values 
			product.DateCreated = DateTime.Now;
			product.CreatedBy = _session.GetCurrentUser().UserID;
			product.DateUpdated = DateTime.Now;
			product.UpdatedBy = _session.GetCurrentUser().UserID;

			_dbContext.Products.Add(product);
			_dbContext.SaveChanges();
		}

		public void Update(Product newProduct)
		{
			var product = GetByID(newProduct.ProductID);

			product.Name = newProduct.Name;
			product.Price = newProduct.Price;

			product.ImageUrl = newProduct.ImageUrl == null ? product.ImageUrl : newProduct.ImageUrl;

			product.DateUpdated = DateTime.Now;
			product.UpdatedBy = _session.GetCurrentUser().UserID;

			_dbContext.Entry(product).State = EntityState.Modified;
			_dbContext.SaveChanges();
		}

		public void Delete(int productID)
		{
			var product = GetByID(productID);

			_dbContext.Products.Remove(product);
			_dbContext.SaveChanges();
		}

		public IList<CategoryProduct> AssignedProductCategories(int productID)
		{
			return _dbContext
				.CategoriesProducts
				.Where(pc => pc.ProductID == productID)
				.AsNoTracking()
				.ToList();
		}
	}
}