﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.CategoriesProducts;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.CoreServices.Products
{
	public interface IProductCoreService
	{
		IList<Product> GetAll();

		Product GetByID(int productID);

		void Insert(Product product);

		void Update(Product product);

		void Delete(int productID);

		IList<CategoryProduct> AssignedProductCategories(int productID);
	}
}