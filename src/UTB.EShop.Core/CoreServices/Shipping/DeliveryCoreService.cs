﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Shipping;

namespace UTB.EShop.Core.CoreServices.Shipping
{
	public class DeliveryCoreService : IDeliveryCoreService
	{
		public IList<Delivery> GetDeliveries()
		{
			return new List<Delivery>
			{
				new Delivery { DeliveryID = 1, Name = "Osobní vyzvednutí" },
				new Delivery { DeliveryID = 2, Name = "PPL" },
				new Delivery { DeliveryID = 3, Name = "DHL" }
			};
		}
	}
}