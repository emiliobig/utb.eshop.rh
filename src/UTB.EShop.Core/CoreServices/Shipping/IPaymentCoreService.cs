﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Shipping;

namespace UTB.EShop.Core.CoreServices.Shipping
{
	public interface IPaymentCoreService
	{
		IList<Payment> GetPayments();
	}
}