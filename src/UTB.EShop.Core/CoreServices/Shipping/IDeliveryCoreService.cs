﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Shipping;

namespace UTB.EShop.Core.CoreServices.Shipping
{
	public interface IDeliveryCoreService
	{
		IList<Delivery> GetDeliveries();
	}
}