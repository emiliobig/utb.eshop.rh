﻿using System.Collections.Generic;
using UTB.EShop.Core.Models.Shipping;

namespace UTB.EShop.Core.CoreServices.Shipping
{
	public class PaymentCoreService : IPaymentCoreService
	{
		public IList<Payment> GetPayments()
		{
			return new List<Payment>
			{
				new Payment { PaymentID = 1, Name = "Online kartou" },
				new Payment { PaymentID = 2, Name = "Dobírkou" },
				new Payment { PaymentID = 3, Name = "Při převzetí" }
			};
		}
	}
}