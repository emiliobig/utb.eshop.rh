﻿using System;
using System.Data.Entity;
using System.Linq;
using UTB.EShop.Core.CoreServices.Cookies;
using UTB.EShop.Core.CoreServices.Products;
using UTB.EShop.Core.EntityFramework;
using UTB.EShop.Core.Models.Addresses;
using UTB.EShop.Core.Models.Carts;

namespace UTB.EShop.Core.CoreServices.Carts
{
	public class CartCoreService : ICartCoreService
	{
		private readonly DataContext _dataContext;
		private readonly IProductCoreService _productCoreService;
		private readonly ICookieCoreService _cookieCoreService;


		// CTOR
		public CartCoreService(DataContext dataContext, IProductCoreService productCoreService, ICookieCoreService cookieCoreService)
		{
			_dataContext = dataContext;
			_productCoreService = productCoreService;
			_cookieCoreService = cookieCoreService;
		}

		// PUBLIC
		public Cart Get()
		{
			var trackingCode = GetOrCreateTrackingCode ();

			var cart = _dataContext
				.Carts
				.Include(e => e.CartItems)
				.Include(e => e.CartItems.Select(ci => ci.Product))
				.Include(e => e.BillingAddress)
				.Include(e => e.ShippingAddress)
				.FirstOrDefault(e => e.UserTrackingCode == trackingCode);

			if (cart == null)
			{
				cart = Insert();
			}

			return cart;
		}

		public CartItem AddToCart(int productID, int amount)
		{
			var cart = Get();
			var cartItem = cart.CartItems.FirstOrDefault(i => i.ProductID == productID);

			if (cartItem == null)
			{
				cartItem = new CartItem
				{
					CartID = cart.CartID,
					ProductID = productID,
					Amount = amount
				};
				cart.CartItems.Add(cartItem);
			}
			else
			{
				cartItem.Amount = amount;	// update of amount, not add; TODO: pokud pridavam z ProductThumbnail, tak se mi aktualni amount v kosiku prepise na 1
			}

			cart.TotalPrice = RecalcTotalPrice(cart);
			_dataContext.SaveChanges();

			return cartItem;
		}

		public void RemoveFromCart(int productID)
		{
			var cart = Get();
			var cartItem = cart.CartItems.FirstOrDefault(i => i.ProductID == productID);

			if (cartItem == null)
			{
				return;
			}

			cart.CartItems.Remove(cartItem);
			cart.TotalPrice = RecalcTotalPrice(cart);

			_dataContext.Entry(cartItem).State = EntityState.Deleted;
			_dataContext.SaveChanges();
		}

		public Address InsertBillingAddress(Address billingAddress)
		{
			var cart = Get();
			cart.BillingAddress = billingAddress;

			_dataContext.SaveChanges();

			return cart.BillingAddress;
		}

		public Address InsertShippingAddress(Address shippingAddress)
		{
			var cart = Get();
			cart.ShippingAddress = shippingAddress;

			_dataContext.SaveChanges();

			return cart.ShippingAddress;
		}

		public int InsertPayment(int paymentID)
		{
			var cart = Get();
			cart.PaymentID = paymentID;

			_dataContext.SaveChanges();

			return cart.PaymentID;
		}

		public int InsertDelivery(int deliveryID)
		{
			var cart = Get();
			cart.DeliveryID = deliveryID;

			_dataContext.SaveChanges();

			return cart.DeliveryID;
		}

		// PRIVATE
		private Cart Insert()
		{
			var cart = new Cart
			{
				UserTrackingCode = GetOrCreateTrackingCode()
			};

			_dataContext.Carts.Add(cart);
			_dataContext.SaveChanges();

			return cart;
		}

		private string GetOrCreateTrackingCode()
		{
			var cookie = _cookieCoreService.GetCookie(Constants.Cookies.CartTrackingCode);

			// Get current cookie
			if (cookie != null)
			{
				return cookie.Value;
			}

			// Create new cookie
			var guid = Guid.NewGuid().ToString();
			_cookieCoreService.Save(Constants.Cookies.CartTrackingCode, guid, DateTime.Now.AddYears(1));
			return guid;
		}

		private double RecalcTotalPrice(Cart cart)
		{
			double totalPrice = 0;

			foreach (var item in cart.CartItems)
			{
				totalPrice += item.Amount * _productCoreService.GetByID(item.ProductID).Price;
			}

			return totalPrice;
		}
	}
}