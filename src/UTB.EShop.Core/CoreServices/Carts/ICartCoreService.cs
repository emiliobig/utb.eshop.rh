﻿using UTB.EShop.Core.Models.Addresses;
using UTB.EShop.Core.Models.Carts;

namespace UTB.EShop.Core.CoreServices.Carts
{
	public interface ICartCoreService
	{
		// Gets instance cart that is belongs to current user. If no cart is found then new cart is created
		Cart Get();

		// Adds product to current cart and returns created instance of CartItem
		CartItem AddToCart(int productID, int amount);

		// Removes product from current cart
		void RemoveFromCart(int productID);

		Address InsertBillingAddress(Address billingAddress);

		Address InsertShippingAddress(Address shippingAddress);

		int InsertPayment(int paymentID);

		int InsertDelivery(int deliveryID);
	}
}