﻿using System.Web;

namespace UTB.EShop.Core.CoreServices.Files
{
	public interface IFileResolver
	{
		string SaveImageAndGetImageUrl(HttpPostedFileBase image);
	}
}