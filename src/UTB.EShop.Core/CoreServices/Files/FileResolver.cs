﻿using System.IO;
using System.Web;
using UTB.EShop.Core.Constants;

namespace UTB.EShop.Core.CoreServices.Files
{
	public class FileResolver : IFileResolver
	{
		private readonly HttpServerUtilityBase _server;

		// CTOR
		public FileResolver(HttpServerUtilityBase server)
		{
			_server = server;
		}

		// PUBLIC
		public string SaveImageAndGetImageUrl(HttpPostedFileBase image)
		{
			var path = Path.Combine(_server.MapPath(Images.ProductPhotosPath), image.FileName);
			image.SaveAs(path);

			return $"{Images.ProductPhotosPath}/{image.FileName}";
		}
	}
}