﻿using System;
using System.Web.Mvc;
using UTB.EShop.Core.CoreServices.Auth;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.Mvc.Permissions
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class HasAccessRoleAttribute : ActionFilterAttribute
	{
		private readonly Roles _authorizedRole;
		private readonly ISessionStateHandler _sessionStateHandler;

		public HasAccessRoleAttribute(Roles authorizedRole)
		{
			_authorizedRole = authorizedRole;
			_sessionStateHandler = DependencyResolver.Current.GetService<ISessionStateHandler>();
		}

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);

			var userRoles = _sessionStateHandler.GetCurrentUser().Roles;
			if (userRoles.HasFlag(_authorizedRole))
			{
				return;
			}

			throw new UnauthorizedAccessException("You don't have permissions.");
		}
	}
}