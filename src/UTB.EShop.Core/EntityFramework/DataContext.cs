﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Carts;
using UTB.EShop.Core.Models.Categories;
using UTB.EShop.Core.Models.CategoriesProducts;
using UTB.EShop.Core.Models.Orders;
using UTB.EShop.Core.Models.Products;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.EntityFramework
{
	public class DataContext : DbContext
	{
		public const string ConnectionString = @"Data Source=localhost; Initial Catalog=eshop.rh; User ID=a5pwt; Password=a5pwta5pwt";

		public DataContext() : base(ConnectionString)
		{
			Database.SetInitializer(new NullDatabaseInitializer<DataContext>());
			_ = System.Data.Entity.SqlServer.SqlProviderServices.Instance;		// ensureDLLIsCopied
		}

		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<Product> Products { get; set; }
		public virtual DbSet<Category> Categories { get; set; }
		public virtual DbSet<CategoryProduct> CategoriesProducts { get; set; }
		public virtual DbSet<Cart> Carts { get; set; }
		public virtual DbSet<CartItem> CartItems { get; set; }
		public virtual DbSet<Order> Orders { get; set; }
		public virtual DbSet<OrderItem> OrderItems { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.HasDefaultSchema(Schemas.Dbo);

			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Configurations.AddFromAssembly(typeof(DataContext).Assembly);
		}
	}
}