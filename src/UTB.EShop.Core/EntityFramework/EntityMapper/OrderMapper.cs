﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class OrderMapper : EntityTypeConfiguration<Order>
	{
		public OrderMapper()
		{
			ToTable(Tables.Orders, Schemas.Web);

			HasMany(e => e.OrderItems)
				.WithRequired(e => e.Order)
				.HasForeignKey(e => e.OrderID)
				.WillCascadeOnDelete();

			HasOptional(e => e.BillingAddress)
				.WithMany()
				.HasForeignKey(e => e.BillingAddressID)
				.WillCascadeOnDelete(false);

			HasOptional(e => e.ShippingAddress)
				.WithMany()
				.HasForeignKey(e => e.ShippingAddressID)
				.WillCascadeOnDelete(false);
		}
	}
}