﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.CategoriesProducts;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class CategoriesProductsMapper : EntityTypeConfiguration<CategoryProduct>
	{
		public CategoriesProductsMapper()
		{
			ToTable(Tables.CategoriesProducts, Schemas.Web);

			HasKey(e => new { e.CategoryID, e.ProductID });

			HasRequired(i => i.Category)
				.WithMany(i => i.Products)
				.HasForeignKey(i => i.CategoryID)
				.WillCascadeOnDelete(true);

			HasRequired(i => i.Product)
				.WithMany(e => e.Categories)
				.HasForeignKey(i => i.ProductID)
				.WillCascadeOnDelete(true);
		}
	}
}