﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Categories;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class CategoriesMapper : EntityTypeConfiguration<Category>
	{
		public CategoriesMapper()
		{
			ToTable(Tables.Categories, Schemas.Web);

			HasKey(e => e.CategoryID);

			Ignore(e => e.IsSelected);
		}
	}
}