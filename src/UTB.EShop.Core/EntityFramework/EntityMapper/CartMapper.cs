﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Carts;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class CartMapper : EntityTypeConfiguration<Cart>
	{
		public CartMapper()
		{
			ToTable(Tables.Carts, Schemas.Web);

			HasMany(e => e.CartItems)
				.WithRequired(e => e.Cart)
				.HasForeignKey(e => e.CartID)
				.WillCascadeOnDelete();

			HasOptional(e => e.BillingAddress)
				.WithMany()
				.HasForeignKey(e => e.BillingAddressID)
				.WillCascadeOnDelete(false);

			HasOptional(e => e.ShippingAddress)
				.WithMany()
				.HasForeignKey(e => e.ShippingAddressID)
				.WillCascadeOnDelete(false);
		}
	}
}