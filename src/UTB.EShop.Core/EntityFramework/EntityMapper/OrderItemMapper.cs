﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Orders;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class OrderItemMapper : EntityTypeConfiguration<OrderItem>
	{
		public OrderItemMapper()
		{
			ToTable(Tables.Orders, Schemas.Web);

			ToTable(Tables.OrderItems, Schemas.Web);

			HasRequired(e => e.Product)
				.WithMany()
				.HasForeignKey(e => e.ProductID)
				.WillCascadeOnDelete();
		}
	}
}