﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Carts;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class CartItemMapper : EntityTypeConfiguration<CartItem>
	{
		public CartItemMapper()
		{
			ToTable(Tables.CartItems, Schemas.Web);

			HasRequired(e => e.Product)
				.WithMany()
				.HasForeignKey(e => e.ProductID)
				.WillCascadeOnDelete();
		}
	}
}