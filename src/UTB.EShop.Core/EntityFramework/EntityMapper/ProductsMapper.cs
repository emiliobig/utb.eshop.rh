﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Products;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class ProductsMapper : EntityTypeConfiguration<Product>
	{
		public ProductsMapper()
		{
			ToTable(Tables.Products, Schemas.Web);

			HasKey(e => e.ProductID);
		}
	}
}