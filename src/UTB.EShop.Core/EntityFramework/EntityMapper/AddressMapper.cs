﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Addresses;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class AddressMapper : EntityTypeConfiguration<Address>
	{
		public AddressMapper()
		{
			ToTable(Tables.Addresses, Schemas.Web);

			HasKey(e => e.AddressID);
		}
	}
}