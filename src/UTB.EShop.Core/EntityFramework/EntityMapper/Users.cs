﻿using System.Data.Entity.ModelConfiguration;
using UTB.EShop.Core.Constants;
using UTB.EShop.Core.Models.Users;

namespace UTB.EShop.Core.EntityFramework.EntityMapper
{
	public class UsersMapper : EntityTypeConfiguration<User>
	{
		public UsersMapper()
		{
			ToTable(Tables.Users, Schemas.App);

			HasKey(e => e.UserID);
		}
	}
}