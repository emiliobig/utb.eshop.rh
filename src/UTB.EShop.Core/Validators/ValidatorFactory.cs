﻿using System;
using Autofac;
using FluentValidation;

namespace UTB.EShop.Core.Validators
{
	public class ValidatorFactory : ValidatorFactoryBase
	{
		private readonly IContainer _container;

		public ValidatorFactory(IContainer container)
		{
			_container = container;
		}

		public override IValidator CreateInstance(Type validatorType)
		{
			// return ResolutionExtensions.ResolveOptionalKeyed<IValidator>(_container, validatorType); // toto mi udelal ReSharper pri presunu z .Web do .Core
			return _container.ResolveOptionalKeyed<IValidator>(validatorType);
		}
	}
}